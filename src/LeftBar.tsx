import { FunctionComponent, useEffect } from 'react'
import logger from './logger'

const log = logger.get('left')

type Props = {
    which: number
}

const Leftbar: FunctionComponent<Props> = ({ which }) => {
    useEffect(() => { log.info('wurde Initialisiert')}, [])
    useEffect(() => { log.info(`which = ${which}`)}, [which])
    return (
        <div style={{fontWeight: 'bold', maxWidth: '200px'}}>
            <h2>Inhalt links {which}</h2>
            Man sieht im Log, wann ich initialisiert werde und wann sich which ändert
        </div>
    )
}

export default Leftbar
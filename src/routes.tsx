import Landing from "./Landing";
import Leftbar from "./LeftBar";
import Navigation from "./Navigation";
import { Route } from "./router/types";

export default [
  {
    path: '/',
    label: <span style={{fontSize: '1.5em'}}>⌂</span>,
    content: {
      main: <Landing />
    }
  },
  {
    path: '/start',
    label: 'start',
    content: {
      main: (
      <>
        <h1>Ich bin die Startseite</h1>
        habe links keinen content
      </>
      ),
      header: <Navigation />
    }
  },
  {
    path: '/withleft',
    label: 'with left',
    content: {
      main: <h1>Bin eine Seite mit linkem content</h1>,
      left: <Leftbar which={1}/>,
      header: <Navigation />
    }
  },
  {
    path: '/withleft2',
    label: 'with left 2',
    content: {
      main: <h1>Bin auch eine Seite mit linkem content</h1>,
      left: <Leftbar which={2}/>,
      header: <Navigation />
    }
  }
] as Route[]

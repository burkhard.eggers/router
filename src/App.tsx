import { FunctionComponent } from "react"
import RouterView from "./router/RouterView"
import styles from './app.module.sass'

const App: FunctionComponent = () =>{
    return (
        <div className={styles.app}>
            <div className={styles.header}>
                <RouterView name="header" />
            </div>
            <div className={styles.body}>
                <div className={styles.left}>
                    <RouterView name="left" />
                </div>
                <div className={styles.main}>
                    <RouterView name="main" />
                </div>
            </div>
        </div>
    )
}



export default App
import { FunctionComponent } from 'react'
import { NavLink } from 'react-router-dom'
import styles from './navigation.module.sass'
import routes from '../routes'
import { Route } from '../router/types'

const Navigation: FunctionComponent = () => { 
    const label = ({path}: Route) => {
        if(path === '*') return 'home'
        return path.replace('/', '')
    }
    const to = ({path}: Route) => {
        if(path === '*') return '/'
        return path
    }
    return (
        <div className={styles.container}>
            {routes.map(route => (
                <NavLink 
                    key={route.path} 
                    to={to(route)} 
                    className={({isActive}) => isActive ? styles.active : ''}>
                        {route.label}
                    </NavLink>    
            ))}
        </ div>
    )
}

export default Navigation
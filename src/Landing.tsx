import { FunctionComponent } from 'react'
import { NavLink } from 'react-router-dom'

const Landing: FunctionComponent = () => (
    <div>
        <NavLink to="/start">zur Startseite</NavLink>
    </div>
)

export default Landing
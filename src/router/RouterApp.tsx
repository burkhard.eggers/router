import React, { FunctionComponent, PropsWithChildren, useContext, useEffect } from 'react';
import {
    createBrowserRouter,
    RouterProvider,
  } from "react-router-dom";

const RouterApp:FunctionComponent<PropsWithChildren> = ({ children }) => {
    const router = createBrowserRouter([{
        path: '*',
        element: children 
    }]);
    return (
          <RouterProvider router={router} />
      );
  }  

  export default RouterApp
import { FunctionComponent } from "react";
import {
    Route as RRRoute,
    Routes
  } from "react-router-dom";
import { Route } from './types'
import routes from "../routes";

type Props = {
    name: string
}

const RouterView: FunctionComponent<Props> = ({name}) => {

    const routes_ = (routes as Route[]).map(({path, content}) => {
        return (
          <RRRoute
            key={path}
            path={path}
            element = {content[name] || ''}
          />
        )
    })

    return (
        <Routes>
          {routes_}
        </Routes>
      );
}

export default RouterView
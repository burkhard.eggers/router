import { ReactNode } from "react"

export type Route = {
    path: string,
    label: ReactNode,
    content: { [key: string]: ReactNode }
}
const paddWithZero = (raw: number) => (`0${raw}`).slice(-2);

const log = (name: string, where: string) => (...args: any[]) => {
  if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    const d = new Date();
    const date = `${paddWithZero(d.getHours())}:${paddWithZero(d.getMinutes())}:${paddWithZero(d.getSeconds())}`;
    const cat = `[${name}]`;
    const spacer = '                                                 '.substring(0, 25 - cat.length);
    const out = `${date} ${cat} ${spacer} [${where}]\t`;
    // this is the only place where console statement is welcome
    // eslint-disable-next-line default-case
    switch (where) {
      case 'log':
        // eslint-disable-next-line no-console
        console.log(out, ...args);
        break;
      case 'info':
        // eslint-disable-next-line no-console
        console.info(out, ...args);
        break;
      case 'warn':
        // eslint-disable-next-line no-console
        console.warn(out, ...args);
        break;
      case 'error':
        // eslint-disable-next-line no-console
        console.error(out, ...args);
        break;
      case 'debug':
        // eslint-disable-next-line no-console
        console.debug(out, ...args);
        break;
    }
  }
};

export type Logger = {
  info: Function,
  error: Function,
  warn: Function,
  debug: Function,
};

const get = (name: string): Logger => ({
  info: log(name, 'info'),
  error: log(name, 'error'),
  warn: log(name, 'warn'),
  debug: log(name, 'debug'),
});

export default {
  get,
};
